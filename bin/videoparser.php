<?php

namespace PHPVideoToolkit;
require_once  dirname( __FILE__ ) . '/phpvideotoolkit-v2-master/autoloader.php';

// $config = new Config(array(

//     'ffmpeg' => '/usr/bin/ffmpeg',

// ), true);

$ffmpeg = new FfmpegParser();
$is_available = $ffmpeg->isAvailable(); // returns boolean
$ffmpeg_version = $ffmpeg->getVersion(); // outputs something like - array('version'=>1.0, 'build'=>null)