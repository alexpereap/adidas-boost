<?php

class Main extends CI_Model{

    public function getUserProfile( $user_id ){
        $q = $this->db->get_where('vw_users_profiles', array('user_id' => $user_id));
        return count($q->row()) > 0 ? $q->row() : false;
    }

    public function saveUser($data){

        $data['creation_date'] = date('Y-m-d');

        $this->db->insert('tbl_users', $data );
        return $this->db->insert_id();
    }

    public function getQuestions(){

        $q = $this->db->get('tbl_questions');
        return $q->result();
    }

    public function getQuestionDetailsById( $question_id ){

        $this->db->order_by('rand()');
        $q = $this->db->get_where('vw_questions_details', array('question_id' => $question_id ));
        return $q->result();
    }

    public function buildQuestionsSet(){

        $questions = $this->getQuestions();

        foreach( (array) $questions as $key => $q ){

            $questions[$key]->answers = $this->getQuestionDetailsById( $q->id );
        }

        return $questions;

    }

    public function getProfileById( $profile_id ){

        $q = $this->db->get_where('tbl_profiles', array('id' => $profile_id ));
        return $q->row();
    }

    public function shareIncrement( $user_id ){
        $this->db->insert('tbl_share_intent', array('fk_user_id' => $user_id) );
    }

    public function logInCmsUser( $userdata ){


        $user = $this->getCmsUserByUsername( $userdata['username'] );

        if( count( $user ) > 0 ){

            $password = $user->password;

            if( $password == $userdata['password'] ){


                $session_data = array(
                    'user_id'   => $user->id,
                    'username'  => $user->username,
                    'logged_in' => TRUE
                    );

                $this->session->set_userdata($session_data);

                return true;
            }
        }

        return false;

    }

    public function getCmsUserByUsername( $username ){
        $q = $this->db->get_where('tbl_cms_user',  array('username' => $username));
        return $q->row();
    }

    public function getSellpoints(){

        $q = $this->db->get('tbl_sellpoints');
        return $q->result();
    }

    public function getUserCount(){

        $query = "SELECT  COUNT(*) as count FROM tbl_users";
        $q = $this->db->query($query);

        return $q->row();
    }

}