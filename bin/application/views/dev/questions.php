<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>

    <h1>Preguntas</h1>

    <form method="POST" action="<?php echo site_url('site/result') ?>"  >
        <ol>
            <?php foreach( $questions as  $q ): ?>
                <li>
                    <?php echo $q->title ?>:
                    <ul style="list-style:none;" >
                        <?php foreach( $q->answers as $a ): ?>
                        <li> <input value="<?php echo $a->profile_id ?>" type="radio" name="question_answers[<?php echo $q->id ?>]" > <?php echo $a->answer_description ?></li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php endforeach; ?>
        </ol>
        <input type="submit" value="Enviar">
    </form>
</body>
</html>