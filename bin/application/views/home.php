<!doctype html>

<!--
    HTML5 Reset: https://github.com/murtaugh/HTML5-Reset
    Free to use
-->

<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->

<base href="<?php echo site_url() ?>">

<head>

    <meta charset="utf-8">

    <!-- Always force latest IE rendering engine (even in intranet) -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Important stuff for SEO, don't neglect. (And don't dupicate values across your site!) -->
    <title>Adidas Boost</title>
    <meta name="author" content="" />
    <meta name="description" content="" />

    <!-- Don't forget to set your site up: http://google.com/webmasters -->
    <meta name="google-site-verification" content="" />

    <!-- Who owns the content of this site? -->
    <meta name="Copyright" content="" />

    <!--  Mobile Viewport
    http://j.mp/mobileviewport & http://davidbcalhoun.com/2010/viewport-metatag
    device-width : Occupy full width of the screen in its current orientation
    initial-scale = 1.0 retains dimensions instead of zooming out if page height > device height
    maximum-scale = 1.0 retains dimensions instead of zooming in if page width < device width (wrong for most sites)
    -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Use Iconifyer to generate all the favicons and touch icons you need: http://iconifier.net -->
    <!--link rel="shortcut icon" href="favicon.ico" /-->

    <!-- concatenate and minify for production -->
    <link rel="stylesheet" href="assets/css/reset.css" />
    <link rel="stylesheet" href="assets/css/style.css?v=<?php echo time() ?>" />
    <link rel="stylesheet" href="assets/css/override.css">

    <!-- Lea Verou's Prefix Free, lets you use un-prefixed properties in your CSS files -->
    <!--script src="assets/js/libs/prefixfree.min.js"></script-->

    <!-- This is an un-minified, complete version of Modernizr.
         Before you move to production, you should generate a custom build that only has the detects you need. -->
    <script src="assets/js/libs/modernizr-2.7.1.dev.js"></script>

    <!-- Application-specific meta tags -->
    <!-- Windows 8: see http://msdn.microsoft.com/en-us/library/ie/dn255024%28v=vs.85%29.aspx for details -->
    <meta name="application-name" content="" />
    <meta name="msapplication-TileColor" content="" />
    <meta name="msapplication-TileImage" content="" />
    <meta name="msapplication-square150x150logo" content="" />
    <meta name="msapplication-square310x310logo" content="" />
    <meta name="msapplication-square70x70logo" content="" />
    <meta name="msapplication-wide310x150logo" content="" />
    <!-- Twitter: see https://dev.twitter.com/docs/cards/types/summary-card for details -->
    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:url" content="">
    <!-- Facebook (and some others) use the Open Graph protocol: see http://ogp.me/ for details -->
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:url" content="" />
    <meta property="og:image" content="" />

</head>

<body>
<form method="POST" id="mainForm" action="<?php echo site_url('site/parse_video') ?>" enctype="multipart/form-data" >
<input type="hidden" name="profile_id" id="profile_id"  value="" >
<div class="wrapper"><!-- not needed? up to you: http://camendesign.com/code/developpeurs_sans_frontieres -->

    <header class="main-head mxWdth768">
        <img src="assets/imgs/header.jpg" />
    </header>

<div class="pattrn">
    <section id="slice01" class="sctn slice active mxWdth768 yllwBg ImgBGregistro">
        <div class="form-padding noBgClr">
            <fieldset class="fldset padding noBrdr">
              <p class="lgnd lgndBlck">REGISTRO</p>
                <div class="center-elmnt">
                    <label class="lbl-inline">
                        <span>Nombres:</span>
                        <input class="input input1" type="text" required placeholder="" name="name" />
                    </label>
                    <label class="lbl-inline">
                        <span>Apellidos:</span>
                        <input class="input input1" type="text" required placeholder="" name="lastname" />
                    </label>
                    <label class="lbl-inline">
                        <span>Email:</span>
                        <input class="input input1" type="email" required placeholder="" name="email" />
                    </label>
                    <label class="lbl-inline">
                      <span>Puntos de venta:</span>
                        <select name="sellpoint" >
                            <option selected></option>
                            <?php foreach( $sellpoints as $s ): ?>
                                <option value="<?php echo $s->id ?>"><?php echo $s->name ?></option>
                            <?php endforeach; ?>
                        </select>
                     </label>
                     <label class="lblElmnts">
                        <input class="" type="radio" name="terms" value="Bike">
                            <label class="lbl" for=""><span></span></label>Acepto <a target="_BLANK" href="<?php echo site_url('terminos-y-condiciones-boost-replays-2.pdf') ?>"> términos, condiciones y exoneración de cargos</a></label>
                    <!--<p class="parrafo p2">*Verifica que tu e-mail este correcto</p>-->

                </div>
            </fieldset>

            <fieldset class="fldset">
                <div class="center-elmnt">
                    <button class="btn btn-lg btn-color1 skew" type="button">
                        <div class="wall"></div>
                        <span class="text">CANCELAR</span>
                    </button>
                    <button class="btn btn-lg btn-color2 skew" id="step1Submit" type="button" data-slice="#slice02-1">
                        <div class="wall"></div>
                        <span class="text">INICIAR</span></button>
                </div>

            </fieldset>
        </div>
        <div class="photo-uploaded-replays icon-replays"></div>
    </section>

    <?php foreach( $questions as $key => $q ): $limit = count( $questions ); ?>
    <section id="slice02-<?php echo $key+1 ?>" class="sctn slice mxWdth768 noPaddng"><!--yllwBg-->
        <div id="questionsWrapper" class="form-padding noBgClr">


            <fieldset id="question_<?php echo $key+1 ?>" class="question fldset ftFldst0<?php echo $key+1 ?>">

                <p class="lgnd lgndRed <?php if( strlen($q->title) >=40  ) echo 'twoLns' ?> "><?php echo $key+1 . '.' . $q->title ?></p>
                  <div class="left-element">

                  <?php foreach( $q->answers as $a ): ?>
                    <label class="lblElmnts">
                        <input class="question-input" type="radio" name="question_answers[<?php echo $q->id ?>]"  value="<?php echo $a->profile_id ?>">
                    <label class="lbl" for=""><span></span></label><?php echo $a->answer_description ?></label>
                  <?php endforeach; ?>
                 </div>
            </fieldset>


            <fieldset class="fldset ftFldst04">
                <div class="center-elmnt">

                    <?php if( $key == 0 ): ?>
                    <button class="btn btn-lg btn-color1 btnSlice lgnd lgndBlck marginRight" data-slice="#slice01" type="button">CAMBIAR</button>
                    <?php else: ?>
                        <button class="btn btn-lg btn-color1 btnSlice lgnd lgndBlck marginRight" data-slice="#slice02-<?php echo $key ?>" type="button">CAMBIAR</button>
                    <?php endif; ?>

                    <?php if( $limit - 1  == $key ): ?>
                    <button id="step2Submit" class="btn btn-lg btn-color2 lgnd lgndRed" data-slice="#slice03-1" type="button">CONTINUAR</button>
                    <?php else: ?>
                    <button  class="btn btn-lg btn-color2 lgnd btnSlice lgndRed" data-slice="#slice02-<?php echo $key+2 ?>" type="button">CONTINUAR</button>
                    <?php endif; ?>
                </div>
            </fieldset>
        </div>
    </section>
    <?php endforeach; ?>


    <section id="slice03-1" class="sctn slice mxWdth768 yllwBg ImgBG1">

        <div class="">

          <div id="mainMedal" class="medalIco "></div>
          <div id="mainProfile" class="labelBig "></div>

          <p class="parrafo p1" id="profileDescription" ><!-- filled by js --></p>
          <div class="center-elmnt toMax90">
            <button class="btn btn-sm btn-color2 grdnt choose-file-btn" file-trigger="1" type="button" >TOMAR FOTO <div file-trigger="1" class="photo-uploaded icon-feedback"></div></button>
            <button class="btn btn-sm btn-color2 grdnt choose-file-btn" file-trigger="2" type="button" >TOMAR FOTO <div file-trigger="2" class="photo-uploaded icon-feedback"></div></button>
            <button class="btn btn-sm btn-color2 grdnt choose-file-btn" file-trigger="3" type="button" >TOMAR FOTO <div file-trigger="3" class="photo-uploaded icon-feedback"></div></button>
            <button class="btn btn-sm btn-color2 grdnt choose-file-btn" file-trigger="4" type="button" >TOMAR FOTO <div file-trigger="4" class="photo-uploaded icon-feedback"></div></button>

            <input type="file" name="photos_1"  class="hidden file-receiver" file-trigger="1" >
            <input type="file" name="photos_2"  class="hidden file-receiver" file-trigger="2" >
            <input type="file" name="photos_3"  class="hidden file-receiver" file-trigger="3" >
            <input type="file" name="photos_4"  class="hidden file-receiver" file-trigger="4" >

            <button id="submitTheForm" class="btn btn-lg btn-color2 skew center-redbutton shadow" type="submit" data-slice=""> <div class="wall"></div>
             <span class="text">ENVIAR</span></button>
          </div>
        </div>
<div class="photo-uploaded-replays3 icon-replays"></div>
    </section>

    <section id="slice03-2" class="sctn slice mxWdth768 yllwBg ImgBG2">

        <div class="">
          <div class="medalIco medal2"></div>

          <div class="labelBig label2"></div>

          <p class="parrafo p1">La ciudad y el acelerado ritmo hacen que el enfoque toda su energía en un objetivo claro e instantáneo, cada segundo cuenta y las distancias cortas son su especialidad.</p>

         <div class="center-elmnt">
            <button class="btn btn-lg btn-color2" type="button">TOMAR FOTO +</button>
          </div>

        </div>

    </section>

    <section id="slice03-3" class="sctn slice mxWdth768 yllwBg ImgBG3">

        <div class="">
          <div class="medalIco medal3"></div>

          <div class="labelBig label3"></div>

          <p class="parrafo p1">Es un runner que prefiere los espacios abiertos y recibir la energía de la naturaleza para llegar a lugares donde otros han fallado en el intento. </p>

         <div class="center-elmnt">
            <button class="btn btn-lg btn-color2" type="button">TOMAR FOTO +</button>
         </div>

        </div>

    </section>

    <!--<section id="slice04" class="sctn slice mxWdth768 yllwBg ImgBG3">

        <form class="">
          <div class="medalIco medal3"></div>



          <p class="parrafo p3">¡TU GIF FUE CREADO CON ÉXITO! </p>

         <div class="center-elmnt">
            <button class="btn btn-lg btn-color3 btnSlice" data-slice="#slice01" type="button">REGRESAR</button>
         </div>

        </form>

    </section>-->

</form>
    <footer class="main-foot">
        <img src="assets/imgs/footer.jpg" />
    </footer>
</div>
</div>

<!-- Grab Google CDN's jQuery. fall back to local if necessary -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write("<script src='assets/js/libs/jquery-1.11.0.min.js'>\x3C/script>")</script>

<!-- this is where we put our custom functions -->
<!-- don't forget to concatenate and minify if needed -->
<script src="assets/js/functions.js"></script>

<!-- Asynchronous google analytics; this is the official snippet.
     Replace UA-XXXXXX-XX with your site's ID and uncomment to enable.

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-XXXXXX-XX', 'auto');
  ga('send', 'pageview');

</script>
-->

</body>
</html>
