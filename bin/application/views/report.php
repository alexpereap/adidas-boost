<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <?php
      if( isset($css_files) )
      foreach($css_files as $file): ?>
      <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>

    <?php
      if( isset($js_files) )
      foreach($js_files as $file): ?>
      <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
</head>
<body>
    <div>
        <h1>Reporte:</h1>
        <p>Usuarios Registrados a la fecha: <?php echo $users_count->count ?></p>
        <?php echo $output; ?>
    </div>
</body>
</html>