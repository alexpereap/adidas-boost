<?php


class Report extends CI_Controller {

    public function index(){

        $this->load->library('grocery_CRUD');
        $this->grocery_crud->set_table('vw_shares_detail');

        $this->grocery_crud->set_primary_key('user_id');

        $this->grocery_crud->callback_column('gif', array($this,'_parse_gif'));
        $this->grocery_crud->callback_column('share_id', array($this,'_parse_share'));

        $this->grocery_crud->display_as('share_id','Ha compartido ?');

        $output = $this->grocery_crud->render();
        $output->users_count = $this->main->getUserCount();


        $this->load->view('report', $output);
    }

    public function _parse_gif( $value, $row ){

        return '<a target="_BLANK" href="' . $value . '" >' . $value . '</a>';
    }

    public function _parse_share( $value, $row ){

        return empty($value) ? 'No' : 'Si';
    }
}