<?php

require_once dirname( __FILE__ ) . '/../libraries/GifCreator-master/src/GifCreator/GifCreator.php';

class Image_test extends CI_Controller {

    public function index(){

        $frames = array(
            file_get_contents( dirname( __FILE__ ) . '/../../test_sequence/0.jpg' ),
            file_get_contents( dirname( __FILE__ ) . '/../../test_sequence/1.jpg' ),
            file_get_contents( dirname( __FILE__ ) . '/../../test_sequence/2.jpg' ),
            file_get_contents( dirname( __FILE__ ) . '/../../test_sequence/3.jpg' )
        );

        $durations = array(30, 30, 30, 30);

        // Initialize and create the GIF !
        $gc = new GifCreator\GifCreator();
        $gc->create($frames, $durations, 0);

        $gifBinary = $gc->getGif();


        $r = file_put_contents( dirname( __FILE__ ) . '/../../test_sequence/animated_picture.gif', $gifBinary);

        krumo($r);


    }

    public function phpsite(){
        phpinfo();
    }
}