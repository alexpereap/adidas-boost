<?php

require_once dirname( __FILE__ ) . '/../libraries/GifCreator-master/src/GifCreator/GifCreator.php';

class Site extends CI_Controller {

     function __construct(){
        parent::__construct();

        if( !$this->session->userdata('logged_in') &&  $this->router->method != 'login' && $this->router->method != 'check_login_credentials' && $this->router->method != 'share_landing' && $this->router->method != 'thanks_for_share'  ){

            redirect( site_url('site/login') );
        }
    }

    public function login(){

        if( $this->session->userdata('logged_in') ){
            redirect( site_url() );
        }

        $this->load->view('login');
    }

    public function index(){

        $data = array(
            'sellpoints' => $this->main->getSellpoints(),
            'questions'  => $this->main->buildQuestionsSet()
        );


        $this->load->view('home', $data);
    }

    public function questions_form(){

        if( $_SERVER['REQUEST_METHOD'] != 'POST' ){
            redirect( site_url() );
            return;
        }

        $this->session->set_userdata('user_personal_data', $_POST );

        $data = array(
            'questions' => $this->main->buildQuestionsSet()
        );

        $this->load->view('dev/questions', $data);

    }

    // analize questions form
    public function result(){

        if( $_SERVER['REQUEST_METHOD'] != 'POST' ){
            redirect( site_url() );
            return;
        }

        $profile_quantity = array();

        // loops the question's answers
        foreach( $this->input->post('question_answers') as $question_id => $profile_id ){

            if( !isset( $profile_quantity[$profile_id] ) )
                $profile_quantity[$profile_id] = 0;

            // increments each time a profile id answer is found, so the array index it's the profile id
            // and the value the number of times an user picked an answer related to that profile
            $profile_quantity[$profile_id]++;
        }

        $tie = false;

        // chekcs if its tie -- if its tie the user profile is id:3 hard runner
        // the logic is ... if some element in array have value of 2 its not a tie
        if ( in_array( 2 , $profile_quantity, true)) {

            $tie = false;
            $profile_id = 3;
        } else {
            $tie = true;
        }


        if( $tie === false ){

            // gets the index of the highest value in the array
            $maxs = array_keys($profile_quantity, max($profile_quantity));
            $profile_id = $maxs[0];
        }

        $profile = $this->main->getProfileById( $profile_id );

        echo json_encode( array(
            'success' => true,
            'profile' => $profile
        ));

        /*$data = array(
            'profile' => $profile
        );

        $this->load->view('dev/video_form', $data);*/

    }

    public function parse_video(){

        global $grabzItApplicationKey;
        global $grabzItApplicationSecret;
        global $grabzItHandlerUrl;

        $this->load->model('model_files');
        $frames = array();

        // save photos (4)
        for( $i = 1; $i<=4; $i++ ){

            if(  $_FILES['photos_' . $i ]['name'] != '' ){


                $rf = $this->model_files->upload_file('photos_' . $i , './uploads', '*' );
                if( $rf['success'] === true ){

                    $file_set[] = $rf['data']['file_name'];

                } else {
                    // handle error
                    $this->uploadError( $rf['error'] );
                    return false;
                }

            } else {

                // handle error
                $this->uploadError( $rf['error'] );
                return false;
            }
        }

        // first photo url to be used in the email
        $first_photo = '';

        foreach( $file_set as $file_name ){

            //648 484
            $output_link = site_url( 'timthumb.php?src=' . site_url('uploads') . '/' . $file_name .'&w=324&h=242&q=100&zc=1' );

            $photo = fopen( $output_link, 'r' );
            $dir_photo = dirname( __FILE__) .'/../../uploads/thumb_' . $file_name;
            file_put_contents( $dir_photo, $photo );

            sleep(1);
            $frames[] = file_get_contents( $dir_photo );

            if( empty( $first_photo ) ){
                $first_photo = site_url('uploads/thumb_' . $file_name);
            }

        }


        // given the frames set builds the gif
        $durations = array(50, 50, 50, 50);

         // Initialize and create the GIF !
        $gc = new GifCreator\GifCreator();
        $gc->create($frames, $durations, 0);

        $gifBinary = $gc->getGif();
        $gif_name = time() . '.gif';

        $r = file_put_contents( dirname( __FILE__ ) . '/../../uploads/' . $gif_name , $gifBinary);
        $gif_url = site_url("uploads") . "/" . $gif_name;

        sleep(3);

        //fetch information in facebook
        $ch = curl_init();
        $intents = 0;

        do{

            $intents++;

            // when caching information on facebook scraper
            // ocasionally an error code 1611008 will come out
            // if this error pops up the gif wasn't cached and it's necessary re scrape the info
            // anyways this always generates an error in the facebook scraper
            // the expected error code is 1660002, if we get this error code it means facebook actually cached the gif image

            $curl_fields = "id=" . $gif_url . "&scrape=true";

            curl_setopt($ch,CURLOPT_URL, "https://graph.facebook.com");
            curl_setopt($ch,CURLOPT_POST, 2);
            curl_setopt($ch,CURLOPT_POSTFIELDS, $curl_fields);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $curl_result = curl_exec($ch);
            $curl_result = json_decode( $curl_result );

            sleep(2);

        } while( $curl_result->error->code == 1611008 );



        curl_close($ch);



        $user_data = array(
            'fk_profile_id'   => $this->input->post('profile_id'),
            'fk_sellpoint_id' => $this->input->post('sellpoint'),
            'name'            => $this->input->post('name'),
            'lastname'        => $this->input->post('lastname'),
            'email'           => $this->input->post('email'),
            'suscribed'       => $this->input->post('suscribe') ? 'yes' : 'no',
            'gif_file'        => $gif_name
        );


        $user_id = $this->main->saveUser( $user_data );
        $user_profile = $this->main->getUserProfile($user_id);

        // MAIL SENDING
        $this->load->model('model_mails');

        $mail_data = array(
            'user_profile' => $user_profile,
            'share_link'   => site_url('site/share_landing/' . $user_id ),
            'main_image'   => $first_photo,
            );


        $mail_content = $this->load->view('mailing/user_mail', $mail_data, true );
        $this->model_mails->singleEmail( $this->input->post('email'), $mail_content, 'Con adidas y Replays descubre qué tipo de corredor eres' , 'activacionesadidasco@gmail.com' );


        $link = "https://www.facebook.com/dialog/feed?app_id=" . "1672418739655262&display=popup&caption=Con adidas y Replays descubre qué tipo de corredor eres&link=" . $gif_url
        . "&redirect_uri=http://www.activacionesadidascolombia.com/boost/site/thanks_for_share/";


        redirect( site_url('site/thanks') );
    }

    private function uploadError( $error = null ){

        if( !empty( $error ) ){
            krumo($error);
        }

        $this->session->set_flashdata('error', 'Lo sentimos, no hemos podido procesar tu imagen.<br/>Verifica que tu imagen sea de formato .jpg, .jpeg o .png.<br/>Guardala tu imagen en alguno de estos formatos y verifica que no pese más de 7MB.');
        // redirect(base_url('landing/register_step_2'));
    }

    public function share_landing( $user_id = null ){

        if( empty($user_id) ){
            die();
        }

        $user_profile = $this->main->getUserProfile($user_id);

        if( $user_profile === false ){
            die();
        }

        // increments share
        $this->main->shareIncrement( $user_id );

        $gif_url = site_url("uploads") . "/" . $user_profile->user_gif_file;

        $link = "https://www.facebook.com/dialog/feed?app_id=" . "1672418739655262&display=popup&caption=Con adidas y Replays descubre qué tipo de corredor eres&link=" . $gif_url
        . "&redirect_uri=http://www.activacionesadidascolombia.com/boost/site/thanks_for_share/";


        redirect( $link );
    }

     public function check_login_credentials(){

        $result = $this->main->logInCmsUser( $_POST );
        echo json_encode( array('result' => $result) );
    }

     public function logInCmsUser( $userdata ){


        $user = $this->getCmsUserByUsername( $userdata['username'] );

        if( count( $user ) > 0 ){

            $password = $this->encrypt->decode( $user->password );

            if( $password == $userdata['password'] ){


                $session_data = array(
                    'user_id'   => $user->id,
                    'username'  => $user->username,
                    'logged_in' => TRUE
                    );

                $this->session->set_userdata($session_data);

                return true;
            }
        }

        return false;

    }

    public function getCmsUserByUsername( $username ){
        $q = $this->db->get_where('tbl_cms_user',  array('username' => $username));
        return $q->row();
    }

    public function thanks(){
        $this->load->view('thanku');
    }

    public function thanks_for_share(){
        $this->load->view('thanks_for_share');
    }
}