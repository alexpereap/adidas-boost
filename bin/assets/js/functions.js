// Browser detection for when you get desparate. A measure of last resort.
// http://rog.ie/post/9089341529/html5boilerplatejs
// sample CSS: html[data-useragent*='Chrome/13.0'] { ... }
//
// var b = document.documentElement;
// b.setAttribute('data-useragent',  navigator.userAgent);
// b.setAttribute('data-platform', navigator.platform);


// remap jQuery to $


(function($){

	/* trigger when page is ready */
	$(document).ready(function (){

		// your functions go here

		/*CHECKS - EFFECTS*/
		$('input[type=radio], input[type=checkbox]').change(function() {
			$("input[type=radio], input[type=checkbox]").removeClass("chckd");
			$(this).addClass("chckd");
			$(this).parents(".left-element").find('.lblElmnts').removeClass('actv').addClass("inctv");
			if ($(this).is(':checked')) {
            	console.log("Transfer");
            	$(this).parent().find("input.chckd").parent().removeClass("inctv").addClass("actv");
        	}
        });

        /*SLICES*/
        	var contentWidth = '-'+($(".slice").width() + 50)+'px';
        	$(".slice:not(.active)").css({position:'absolute', left:contentWidth});
        	console.log($(window).width()+'px')
        		// $(".btnSlice").click(function(event){
                $(document).on("click", ".btnSlice", function(event){
                    event.preventDefault();

                    var $_sliceID = $( $(this).attr('data-slice') );
                    if ($($_sliceID).hasClass('active')) { return; };
                    console.log($_sliceID);
                    $_sliceID.addClass('active anmtd').animate({left:0}, function(){ $(this).removeClass("anmtd"); });
                    $(".slice:not(.anmtd)").removeClass('active anmtd').animate({left: '-' + $(window).width() }, function(){
                            $(this).css('left',contentWidth);
                    });

                });

        		// })

        	console.log(contentWidth);

            // validators
            $("#step1Submit").click(function(){

                // btnSlice

                if( !$(this).hasClass('btnSlice') ){

                    if( $("input[name='name']").val() == '' ){
                        alert('Por favor especifica los nombres.');
                        return false;
                    }

                    if( $("input[name='lastname']").val() == '' ){
                        alert('Por favor especifica los apellidos.');
                        return false;
                    }

                    if( !validateEmail(  $("input[name='email']").val() )  ){
                        alert('Por favor especifica una dirección de email valida.');
                        return false;
                    }

                    if( $("select[name='sellpoint']").val() == '' ){
                        alert('Por favor especifica el punto de venta.');
                        return false;
                    }

                    if( !$("input[name='terms']").is(':checked')  ){
                        alert('Por favor acepta términos, condiciones y exoneración de cargos.');
                        return false;
                    }

                    $(this).addClass('btnSlice');
                    $(this).click();
                }

            });


            var questionsQty = $("fieldset.question").length;
            var current_question = 1;
            var clicked_answers = 0;
            var lastQuestion = false;
            $("#questionsWrapper input[type='radio']").click(function(){



                if( lastQuestion === false  ){
                    current_question++;
                }


                if( current_question == questionsQty ){

                    console.log("last");
                    lastQuestion = true;
                }

                clicked_answers++;
                console.log(clicked_answers);

            });

            $("#step2Submit").click(function(){

                var $theBtn = $(this);
                var validateError = false;

                if( !$(this).hasClass('btnSlice') ){

                    if( clicked_answers < questionsQty  ){
                        alert("Por favor responde todas las preguntas");
                        return false;
                    }

                     $.ajax({

                        url  : 'site/result',
                        type : 'post',
                        data : $("#mainForm").serialize(),
                        dataType : 'json',
                        success:function(result){

                            if( result.success === true ){

                                console.log(result);

                                switch( parseInt(result.profile.id) ){

                                    // eco runner
                                    case 1:
                                        $("#mainMedal").addClass('medal3');
                                        $("#mainProfile").addClass('label3');
                                        break;

                                    // Action runner
                                    case 2:
                                        $("#mainMedal").addClass('medal2');
                                        $("#mainProfile").addClass('label2');
                                        break;

                                    // hard runner;
                                    case 3:
                                        $("#mainMedal").addClass('medal1');
                                        $("#mainProfile").addClass('label1');
                                        break;

                                }

                                $("#profile_id").val( parseInt( result.profile.id ) );
                                $("#profileDescription").text( result.profile.description );

                                $theBtn.addClass('btnSlice');
                                $theBtn.click();



                            } else {

                                alert("Ha ocurrido un problema con la conexión de internet, por favor intentalo de nuevo.");
                            }
                        }
                    });
                }

            });

        // input files handler

        $(".choose-file-btn").click(function(){

            var file_trigger = $(this).attr('file-trigger');

            $("input[name='photos_" + file_trigger + "']").click();
        });

        $(".file-receiver").change(function(event){

            var file_trigger = $(this).attr('file-trigger');

            if( $(this).val() != '' ){
                $(".photo-uploaded[file-trigger='" + file_trigger + "']").removeClass('icon-feedback');
                $(".photo-uploaded[file-trigger='" + file_trigger + "']").addClass('icon-feedback-uploaded');
            } else{
                $(".photo-uploaded[file-trigger='" + file_trigger + "']").addClass('icon-feedback');
                $(".photo-uploaded[file-trigger='" + file_trigger + "']").removeClass('icon-feedback-uploaded');
            }


        });

        // send form

        $("#submitTheForm").click(function(e){

            var fileError = false;

            $(".file-receiver").each(function(index){

                if( $(this).val() == '' ){

                    alert("Por favor selecciona la foto " + (index +1) );

                    fileError = true;
                    return false;
                }

            });

            if( fileError === true ){

                e.preventDefault();
                return false;
            }
        });

	}); // DREADY END


	/* optional triggers

	$(window).load(function() {

	});

	$(window).resize(function() {

	});

	*/

})(window.jQuery);

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}